output "dev_bucket_name" {
  value = aws_s3_bucket.dev_bucket.bucket
  description = "The name of the dev S3 bucket."
}

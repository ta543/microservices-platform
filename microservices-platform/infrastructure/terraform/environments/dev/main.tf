provider "aws" {
  region = var.region
}

resource "aws_s3_bucket" "dev_bucket" {
  bucket = "my-app-dev-${random_id.this.hex}"
  acl    = "private"
}

resource "random_id" "this" {
  byte_length = 8
}

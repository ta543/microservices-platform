provider "aws" {
  region = var.region
}

resource "aws_s3_bucket" "staging_bucket" {
  bucket = "my-app-staging-${random_id.this.hex}"
  acl    = "private"
}

resource "random_id" "this" {
  byte_length = 8
}

output "staging_bucket_name" {
  value = aws_s3_bucket.staging_bucket.bucket
  description = "The name of the staging S3 bucket."
}

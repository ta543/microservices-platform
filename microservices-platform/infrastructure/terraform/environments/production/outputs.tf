output "production_bucket_name" {
  value = aws_s3_bucket.production_bucket.bucket
  description = "The name of the production S3 bucket."
}

resource "aws_rds_cluster" "main" {
  cluster_identifier = var.cluster_identifier
  engine             = var.engine
  master_username    = var.master_username
  master_password    = var.master_password
  backup_retention_period = 5
  preferred_backup_window = "07:00-09:00"
}

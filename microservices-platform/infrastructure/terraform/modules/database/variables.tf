variable "cluster_identifier" {
  description = "The identifier for the RDS cluster."
  type        = string
}

variable "engine" {
  description = "The database engine to use."
  type        = string
}

variable "master_username" {
  description = "Username for the master DB user."
  type        = string
}

variable "master_password" {
  description = "Password for the master DB user."
  type        = string
  sensitive   = true
}
